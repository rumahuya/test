package com.testaplikasi.testkuis;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import im.delight.android.webview.AdvancedWebView;

public class Quiz extends AppCompatActivity {
    private AdvancedWebView webView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        getSupportActionBar().hide();
//        getWindow().setFlags(
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
//        );
        View decor = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        String versiobb = "7";
        File obb = new File("sdcard/Android/obb/"+getPackageName()+"/main."+versiobb+"."+getPackageName()+".obb");
        obb.delete();
//
//        Intent iin = getIntent();
//        Bundle bb = iin.getExtras();
//        String file = bb.getString("file");

        setTitle(getString(R.string.app_name));

//        Cipher cipher = null;
//        try {
//            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        }
//        String ps2 = readConfig();
//        String key = "dTUxNzE3MzBZQVAwMzA4MTg=";
//        Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
//        byte[] tmp2 = Base64.decode(ps2, Base64.DEFAULT);
//        try {
//            cipher.init(Cipher.DECRYPT_MODE, aesKey);
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        }
//        String decrypted = null;
//        try {
//            decrypted = new String(cipher.doFinal(tmp2),"UTF-8");
//        } catch (BadPaddingException e) {
//            e.printStackTrace();
//        } catch (IllegalBlockSizeException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

//        String base = "file:///sdcard/Android/data/"+getPackageName()+"/";
        String base = "file:///android_asset/";

        progressBar = findViewById(R.id.progressbar);
        webView = findViewById(R.id.web);
        webView.setWebViewClient(new Browser_home());
        webView.setWebChromeClient(new MyChrome());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setOnLongClickListener(v -> true);
        webView.setLongClickable(false);
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            webView.getSettings().setAllowFileAccess(true);
            webView.loadHtml(readConfig(), base);
        }
    }

    public String readConfig() {
        try {
            InputStream test = getAssets().open("index.html");

            //FileInputStream fis = new FileInputStream(new File(myFile));
            InputStreamReader isr = new InputStreamReader(test, "UTF-8");
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("");
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            return "notfound";
        } catch (UnsupportedEncodingException e) {
            return "";
        } catch (IOException e) {
            return "";
        }
    }

    private class Browser_home extends WebViewClient {
        Browser_home() {
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            setTitle(view.getTitle());
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }
    }
    private class MyChrome extends WebChromeClient {
        private View mCustomView;
        private CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        MyChrome() {}

        public Bitmap getDefaultVideoPoster()
        {
            if (mCustomView == null) {
                return null;
            }
            return BitmapFactory.decodeResource(getApplicationContext().getResources(), 2130837573);
        }

        public void onHideCustomView()
        {
            ((FrameLayout)getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
        }

        public void onShowCustomView(View paramView, CustomViewCallback paramCustomViewCallback)
        {
            if (this.mCustomView != null)
            {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout)getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getWindow().getDecorView().setSystemUiVisibility(3846);
        }
    }
}
